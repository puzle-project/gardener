class ProjectConfig:

    def __init__(self, seed_name: str="", seed_location="", project_name: str="", destination=""):

        self.project_name = project_name
        self.seed_name = seed_name
        self.seed_location = seed_location
        self.destination = destination
