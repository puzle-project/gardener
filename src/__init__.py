from .cli import cli
from .core import generate
from .config import *
from .abstract_tree import *
from .generation import *
from .repository import *


def main():
    cli()