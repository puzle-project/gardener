init:
	pip install -r src/requirements.txt
	pip install pipreqs
clean:
	rm --force --recursive  dist
lint:
	pylint .\src\__init__.py
freeze:
	pipreqs src --force
install:
	init
	python setup.py install
run:
	python src/__init__.py
pip-upload:
	python setup.py bdist_wheel --universal
	python setup.py sdist
	twine upload dist/*
